package de.hsog.lucene.demo.extractor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class HTMLDocument implements Serializable  {

	public int crawlerId;
	public String url;
	public String title;
	public String keywords;
	public String description;
	public Map<String, String> links = new HashMap<String, String>();
	public String content;

	public int getCrawlerId() {
		return crawlerId;
	}

	public void setCrawlerId(int crawlerId) {
		this.crawlerId = crawlerId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, String> getLinks() {
		return links;
	}

	public void setLinks(Map<String, String> links) {
		this.links = links;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
