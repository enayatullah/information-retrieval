package de.hsog.lucene.demo.crawler;

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.regex.Pattern;

import de.hsog.lucene.demo.extractor.HTMLDocument;
import de.hsog.lucene.demo.extractor.HTMLExtractor;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author Yasser Ganjisaffar <lastname at gmail dot com>
 * @author Stephan Trahasch
 */
public class BasicCrawler extends WebCrawler {

	private String baseDir = "C:/Lucene/Crawler/Docs/";
	private final static Pattern FILTERS = Pattern
			.compile(".*(\\.(css|js|bmp|gif|jpe?g"
					+ "|png|tiff?|mid|mp2|mp3|mp4"
					+ "|wav|avi|mov|mpeg|ram|m4v|pdf"
					+ "|rm|smil|wmv|swf|wma|zip|rar|gz))$");

	/**
	 * Wir crawlen nur Seiten der HS Offenburg
	 * Grafiken, Multimedia-Daten etc. interessieren
	 * uns auch nicht ...
	 * Siehe Pattern Filters
	 */
	@Override
	public boolean shouldVisit(WebURL url) {
		String href = url.getURL().toLowerCase();
		return !FILTERS.matcher(href).matches()
				&& (href.startsWith("http://www.hs-offenburg.de/") || href.startsWith("http://ei.hs-offenburg.de/"));
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */
	@Override
	public void visit(Page page) {

		HTMLDocument hdoc = new HTMLDocument();
		int docid = page.getWebURL().getDocid();
		String url = page.getWebURL().getURL();
		String domain = page.getWebURL().getDomain();
		String path = page.getWebURL().getPath();
		String subDomain = page.getWebURL().getSubDomain();
		String parentUrl = page.getWebURL().getParentUrl();

		System.out.println("Docid: " + docid);
		System.out.println("URL: " + url);
		System.out.println("Domain: '" + domain + "'");
		System.out.println("Sub-domain: '" + subDomain + "'");
		System.out.println("Path: '" + path + "'");
		System.out.println("Parent page: " + parentUrl);

		if (page.getParseData() instanceof HtmlParseData) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();

			// In hdoc werden alle relevanten Informationen f�r den Index
			// gespeichert. Dieses Dokument muss sp�ter dem Indexer
			// �bergeben werden.

			try {
				hdoc = HTMLExtractor.extractFrom(page.getWebURL().toString());
				hdoc.setContent(htmlParseData.getText());
				hdoc.setCrawlerId(docid);
				writeDoc(hdoc);
			} catch (Exception e) {
				System.out.println("Could not parse " + url);
				e.printStackTrace();
			}

			// TODO

			/**
			 * nicht notwendig String text = htmlParseData.getText(); String
			 * html = htmlParseData.getHtml(); List<WebURL> links =
			 * htmlParseData.getOutgoingUrls();
			 *
			 * System.out.println("Text length: " + text.length());
			 * System.out.println("Html length: " + html.length());
			 * System.out.println("Number of outgoing links: " + links.size());
			 **/
		}

		System.out.println("=============");
	}

	/**
	 * Wir speichern das HTML-Dokument
	 * als serialisierte Java-Klasse des Typs
	 * HTMLDocument.java
	 * Somit kann der Indexer die Informationen
	 * in HTMLDocument.java bereits nutzen.
	 * @param doc
	 */
	public void writeDoc(HTMLDocument doc) {

	    Random random = new Random();
	    int rand = random.nextInt(1000);
	    String fileName = "doc" + doc.getCrawlerId()+"_"+rand+".ser";

		OutputStream fos = null;

		try {
			fos = new FileOutputStream(baseDir + fileName);
			ObjectOutputStream o = new ObjectOutputStream(fos);
			o.writeObject(doc);
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			try {
				fos.close();
			} catch (Exception e) {
				System.err.println(e);
			}
		}

	}
}
