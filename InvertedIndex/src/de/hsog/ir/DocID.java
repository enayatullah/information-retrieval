package de.hsog.ir;

public class DocID {

	int id;

	public DocID(int id) {
		this.id = id;
	}

	public boolean equals(Object obj) {
		DocID docid = (DocID) obj;
		return docid.id == this.id;
	}

	public String toString() {
		return "" + id;
	}

}
