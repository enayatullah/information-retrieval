package de.hsog.ir;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Run {

	static public void main(String args[]) throws FileNotFoundException,
			IOException {

		String workingDirectory = "C://Temp//";
		String stopWordFile = "stopwords.txt";
		InvertedIndex tex = new InvertedIndex(workingDirectory + stopWordFile);
		//Parser simpleparser = new Parser();
		int i = 0;

		File[] entries = new File(workingDirectory).listFiles();
		for (File filename : entries) {
			String document = getFileContent(filename);
			System.out.println("Processing document:" + filename);
			//tex.extractFromHTMLString(document, i++);
			tex.extractTokens(document, i++);
		}

		tex.saveToFile(workingDirectory + "index.txt");
	}

	static String getFileContent(File filename) throws FileNotFoundException,
			IOException {

		StringBuffer result = new StringBuffer();

		FileReader fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr);
		String eachLine = br.readLine();

		while (eachLine != null) {
			result.append(eachLine);
			result.append("\n");
			eachLine = br.readLine();
		}
		System.out.println(result.toString());

		return result.toString();
	}
}
