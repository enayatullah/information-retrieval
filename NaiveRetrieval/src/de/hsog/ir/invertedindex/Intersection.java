package de.hsog.ir.invertedindex;

import java.util.SortedSet;

/*
 * Wir implementieren die Schnittmengenbildung als Interface mit zwei Implementierungen die
 * man verwenden kann.
 */
public interface Intersection {

    public SortedSet<Integer> of(SortedSet<Integer> pl1, SortedSet<Integer> pl2);

}
