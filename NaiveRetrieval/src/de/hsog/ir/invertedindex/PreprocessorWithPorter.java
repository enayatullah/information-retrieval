package de.hsog.ir.invertedindex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.tartarus.snowball.ext.porterStemmer;

/*
 * Einfacher Preprocessor der Spezialfaelle extrahiert und den Rest simpel splittet und sortierte
 * Types zurueckgibt.
 */
final class PreprocessorWithPorter {
	/*
	 * Verfuegbare Musterbeschreibungen fuer zu extrahierenden Formate. Der
	 * Vorteil der Verwendung einer Enum (anstelle von Konstanten) liegt darin,
	 * dass so alle in dieser Enum enthaltenen Muster im Default-Konstruktor des
	 * Preprocessor ausgelesen werden koennen.
	 */
	enum ExtractionPattern {
		/*
		 * Regulaerer Ausdruck fuer einfache Telefonnummern (0221-4701751),
		 * Versionsnummern (8.04), Geldbetraege (3,50) und Uhrzeiten (15:15).
		 */
		COMPOUND_NUMBER("\\d+[-.,:]\\d+"),
		/*
		 * Regulaerer Ausdruck fuer einfache Zahlen, wenn man die etwa von
		 * obigen unterscheiden will.
		 */
		SIMPLE_NUMBER("\\d+"),
		/*
		 * Emailadressen fuer einige Domains, mit Unterstuetzung von Punkten im
		 * Domainnamen als Beispiel was man sonst so mit regulaeren Ausdruecken
		 * machen kann.
		 */
		EMAIL("[^@\\s]+@.+?\\.(de|com|eu|org|net)");

		String regex;

		ExtractionPattern(String regularExpression) {
			this.regex = regularExpression;
		}
	}

	/*
	 * Ein Unicode-wirksamer Ausdruck fuer "Nicht-Buchstabe", der auch Umlaute
	 * beruecksichtigt; die einfache (ASCII) Version ist: "\\W"
	 */
	private static final String UNICODE_AWARE_DELIMITER = "[^\\p{L}]";
	private List<ExtractionPattern> specialCases;
	private String delimiter;

	/*
	 * Konstruktor mit Argumenten, um speziell konfigurierte Vorverarbeitung zu
	 * ermoeglichen (z.B. nicht alle Muster extrahieren, einen speziellen
	 * Delimiter verwenden).
	 */
	PreprocessorWithPorter(List<ExtractionPattern> specialCases,
			String tokenDelimiter) {
		this.specialCases = specialCases;
		this.delimiter = tokenDelimiter;
	}

	PreprocessorWithPorter() {
		specialCases = new ArrayList<ExtractionPattern>();
		/*
		 * Im no-arg constructor verwenden wir alle patterns (einer der Vorteile
		 * von enums: man kann darueber iterieren, im Gegensatz zu Konstanten):
		 */
		for (ExtractionPattern p : ExtractionPattern.values()) {
			specialCases.add(p);
		}
		delimiter = UNICODE_AWARE_DELIMITER;
	}

	/*
	 * Teilt einen Text in einzelne Terme auf 
	 * userPorter = true -> Terme werden mit PorterStemmer auf "Normalform" reduziert
	 * Implementieren Sie hier den Einsatz einer Stopp-Wort-Liste
	 */
	public List<String> process(String text, boolean usePorter) {
		SortedSet<String> result = new TreeSet<String>();
		
		/* Einheitliches lower-casing, wie in der Theorie besprochen: */
		text = text.toLowerCase();
		
		for (ExtractionPattern p : specialCases) {
			Pattern pattern = Pattern.compile(p.regex);
			Matcher matcher = pattern.matcher(text);
			while (matcher.find()) {
				String group = matcher.group();
				result.add(group);
				text = text.replace(group, "");
				/*
				 * Hier muss man aufpassen: replaceAll nimmt einen regulaeren
				 * Ausdruck als erstes Argument. Das ist nicht was wir wollen,
				 * weil die extrahierten Muster als regulaere Ausdruecke wieder
				 * etwas anderes bedeuten, oder erst gar keine gueltigen
				 * regulaeren Ausdruecke sind. Daher nehmen wir hier die
				 * replace-Methode, die einen einfachen String sucht und
				 * ersetzt.
				 */
			}
		}
		
		/* Den Rest splitten wir normal, und filtern leere Strings */
		List<String> list = Arrays.asList(text.split(delimiter));
		for (String s : list) {
			if (s.trim().length() > 0) {
				result.add(s.trim());
			}
		}
		
		// ohne oder mit PorterStemmer?
		if (!usePorter){
			return new ArrayList<String>(result);	
		}
		else {
			// hier kommt nun der Porter-Stemmer zum Einstaz
			SortedSet<String> stemmedResult = new TreeSet<String>();
			porterStemmer stemmer = new porterStemmer();

			for (String type : result) {
				String w = type;
				stemmer.setCurrent(w);
				stemmer.stem();
				w = stemmer.getCurrent();

				stemmedResult.add(w);
			}
			return new ArrayList<String>(stemmedResult);
		}
	}

}
