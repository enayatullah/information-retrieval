package de.hsog.ir.invertedindex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class IntersectionBook implements Intersection {

	/*
	 * Implementierung der Listen-Intersection, die die Sortierung der Listen
	 * ausnutzt, fast Zeile-fuer-Zeile umgesetzt wie in Manning et al. 2008, S.
	 * 11, beschrieben. Hier haben wir einen Algorithmus, der fuer unseren
	 * Anwendungsfall spezialisiert ist und positive Eigenschaften hat. Folien
	 * siehe Kapitel 2, Seite 28
	 */
	public SortedSet<Integer> of(SortedSet<Integer> pl1, SortedSet<Integer> pl2) {
		List<Integer> answer = new ArrayList<Integer>(); // answer = Ergebnis
		Iterator<Integer> i1 = pl1.iterator(); // pl1 = postingList1
		Iterator<Integer> i2 = pl2.iterator(); // pl2 = postingList2
		Integer p1 = nextOrNull(i1);
		Integer p2 = nextOrNull(i2);

		while (p1 != null && p2 != null) {

			// TODO: Implementieren Sie hier den Pseudocode zum Mergen der
			// PostingLists
			// siehe Kapitel 2 Folie 27
			// Loesung
			if (p1.equals(p2)) {
				answer.add(p1);
				p1 = nextOrNull(i1);
				p2 = nextOrNull(i2);
			} else {
				if (p1 < p2) {
					p1 = nextOrNull(i1);
				} else {
					p2 = nextOrNull(i2);
				}
			}

		}
		Collections.sort(answer);
		return new TreeSet<Integer>(answer);
	}

	/* Ein wenig muessen wir uns verbiegen um nah am Pseudocode zu bleiben: */
	private Integer nextOrNull(Iterator<Integer> i1) {
		return i1.hasNext() ? i1.next() : null;
	}
}
